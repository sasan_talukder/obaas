# README #

Welcome to the OBAAS case study solution

### What is this repository for? ###

* OBAAS application site 
* Version 1.0

### Prerequisite ###

* Python version 3.0 or above 
* Django version 1.9 or above
* Mysql
* Python MySql client (1.3.10)
* Pip

### How do I set up? ###

* Clone the OBAAS case study solution repository into your local machine using the following command
```
$ git clone https://sasan_talukder@bitbucket.org/sasan_talukder/obaas.git
```
* Create a database schema called "djangodb" in your local instance of the MySQL server

* Configure the MySQL database variables located on the OBAAS/settings.py file 

```
#!python

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'djangodb',                                                     # Defining the MySQL database schema
        'USER': 'root',                                                         # MySQL database user name for localhost
        'PASSWORD': 'password',                                                 # MySQL database password for localhost 'root'
        'HOST': 'localhost',
        'PORT': '3306',                                                         # MySQL database port 
    }
}
```

* Run the Django migrations using the following commands

```
$ python manage.py makemigrations 
$ python manage.py migrate 
```

* Once the migrations are successful run the Django fixtures script to populate the database tables with relevant data. Use the following command to run the fixtures 
```
$ python manage.py loaddata bankApp/fixtures/seed_data.json 
```

* Run the django server using the following command 
```
$ python manage.py runserver
```

* Access the site using any modern web-browser. By default, the runserver command starts the development server on the internal IP at port 8000.




