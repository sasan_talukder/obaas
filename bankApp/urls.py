from django.conf.urls import url
from . import views

# Defining URL pattenrs to route different views on the site
app_name = 'bank'
urlpatterns = [
    url(r'^account-balance/$', views.checkBalance, name='account-balance'),
    url(r'^service-request/$', views.serviceRequest, name='service-request'),
    url(r'^pay-bill/$', views.payBill, name='pay-bill'),
    url(r'^close-account/$', views.closeAccount, name='close-account'),
    url(r'^create-account/$', views.createAccount, name='create-account'),
]
