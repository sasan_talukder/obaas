from django import forms
from .models import NEWACCOUNT
from django import forms

# NEWACCOUNT model form to create the input fields for account creation
class RegisterForm(forms.ModelForm):
    # Customising the Password field in the model to provide additional security
    PASSWORD = forms.CharField(widget=forms.PasswordInput)
    REPASSWORD = forms.CharField(widget=forms.PasswordInput, label='Confirm Password')
    # Disabling the CATEGORY field
    CATEGORY = forms.CharField(disabled=True, initial = 'U')

    class Meta:
        model = NEWACCOUNT
        # Changing the labels for each form fields
        labels = {
        "USERNAME": "Username",
        "PHONE": "Phone",
        "ADDRESS": "Address",
        "POSTCODE": "Postcode",
        "STATE": "State",
        }

        # Defining which model fields needs to be rendered in the form
        fields = ['USERNAME', 'PASSWORD', 'REPASSWORD', 'PHONE', 'ADDRESS', 'POSTCODE', 'STATE', 'CATEGORY']

# Login form to create the input fields for general account login
class LoginForm(forms.ModelForm):
    PASSWORD = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = NEWACCOUNT
        labels = {
        "USERNAME": "Username",
        "ACCOUNTNO": "Account Number",
        }
        fields = ['USERNAME', 'PASSWORD', 'ACCOUNTNO']

    # Overriding the generic unique validation function for the ACCOUNTNO field
    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError as e:
            try:
                del e.error_dict['ACCOUNTNO'] #if ACCOUNTNO unique validation occurs it will be omitted and form.is_valid() method pass
            except:
                pass                          # if any other error occours
            self._update_errors(e)

# Login form for Bill Pay page
class BillForm(forms.ModelForm):
    PASSWORD = forms.CharField(widget=forms.PasswordInput)
    # Adding an extra field in the form for the Biller types
    BILLER_CHOICES = (
        ('Core Energy', 'Core Energy'),
        ('Telcom Australia', 'Telcom Australia'),
    )
    BILLER = forms.ChoiceField(label='Biller',choices = BILLER_CHOICES)
    class Meta:
        model = NEWACCOUNT
        labels = {
        "USERNAME": "Username",
        "ACCOUNTNO": "Account Number",
        "AMOUNT": "Amount",
        }
        fields = ['USERNAME', 'PASSWORD', 'ACCOUNTNO','AMOUNT','BILLER']

    # Overriding the generic unique validation function for the ACCOUNTNO field
    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError as e:
            try:
                del e.error_dict['ACCOUNTNO'] #if ACCOUNTNO unique validation occurs it will be omitted and form.is_valid() method pass
            except:
                pass                          # if any other error occours
            self._update_errors(e)

# Login form for Service Request page
class ServiceForm(forms.ModelForm):
    PASSWORD = forms.CharField(widget=forms.PasswordInput)
    SERVICE_CHOICES = (
        ('CB', 'Cheque Book'),
    )
    TYPE = forms.ChoiceField(label='Request Service',choices = SERVICE_CHOICES)
    class Meta:
        model = NEWACCOUNT
        labels = {
        "USERNAME": "Username",
        "ACCOUNTNO": "Account Number",
        }
        fields = ['USERNAME', 'PASSWORD', 'ACCOUNTNO', 'TYPE']

    # Overriding the generic unique validation function for the ACCOUNTNO field
    def validate_unique(self):
        exclude = self._get_validation_exclusions()
        try:
            self.instance.validate_unique(exclude=exclude)
        except forms.ValidationError as e:
            try:
                del e.error_dict['ACCOUNTNO'] #if ACCOUNTNO unique validation occurs it will be omitted and form.is_valid() method pass
            except:
                pass                          # if any other error occours
            self._update_errors(e)
