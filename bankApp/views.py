from django.shortcuts import render
from .models import NEWACCOUNT
from .models import CHEQUEBOOK
from .forms import RegisterForm, LoginForm, ServiceForm, BillForm
from random import randint
import hashlib
import datetime
from decimal import Decimal

# Defining a view to handle Account Creation logic
def createAccount(request):
    form = RegisterForm(request.POST or None)                                   # Defining the account cretion form
    if request.method == 'POST':                                                # If the form has been submitted
        form = RegisterForm(request.POST or None)
        if form.is_valid():                                                     # Checking if the submitted form is valid
            data = form.cleaned_data                                            # Cleaning any corrupt form data
            name = data['USERNAME']                                             # Obtaining data from the submitted form
            rawPassword = data['PASSWORD']
            password = hashlib.md5(rawPassword.encode('utf-8')).hexdigest()     # Hashing the user password
            confirmPass = data['REPASSWORD']
            phoneNumber = data['PHONE']
            street = data['ADDRESS']
            state = data['STATE']
            postcode = data['POSTCODE']
            balance = 500

            # Generating unique accountNumber for the DB
            flag = 1
            while (flag == 1):
                accountNumber = randint(100, 999)
                if NEWACCOUNT.objects.filter(ACCOUNTNO=accountNumber).exists():
                    flag = 1
                else:
                    flag = 0
            try:                                                                # Performing backend form validation for any invalid data
                if rawPassword != confirmPass:
                    raise ValueError('Passwords do not match. Please ensure password and confirm password are the same!')
                if len(str(phoneNumber)) > 11:
                    raise ValueError('Invalid Phone number! Must be less than 11 digits')
                elif (len(str(postcode)) > 4 or len(str(postcode)) < 4) :
                    raise ValueError('Invalid Postcode! Must contain 4 digits')
            except ValueError as error:                                         # Raising an exception in case of invalid data
                context = {
                    "form": form,
                    "error_message": error.args[0],
                }
                return render(request, 'bank/create_account.html', context)
            else:                                                               # Saving the data in the database
                new_user = NEWACCOUNT(USERNAME = name, PASSWORD = password, REPASSWORD = password, PHONE = phoneNumber, AMOUNT = balance, ADDRESS = street, STATE = state, POSTCODE = postcode, ACCOUNTNO = accountNumber)
                new_user.save()
                context = {
                    "accountNumber": accountNumber,
                }
                return render(request, 'bank/register-sucess.html', context)
    context = {
        "form": form,
    }
    return render(request, 'bank/create_account.html', context)                 # Returning an empty form

# Defining a view to display the account balance for a specific user
def checkBalance(request):
    form = LoginForm(request.POST or None)
    if request.method == 'POST': # If the form has been submitted
        form = LoginForm(request.POST or None)
        if form.is_valid():                                                     # Checking if the submitted form is valid
            data = form.cleaned_data
            name = data['USERNAME']
            rawPassword = data['PASSWORD']
            password = hashlib.md5(rawPassword.encode('utf-8')).hexdigest()
            accountNumber = data['ACCOUNTNO']
            isValidUser = authenticate(username = name, password = password, accountNumber = accountNumber)     # Checking if the login credentials are valid
            try:
                if isValidUser:
                    querySet = NEWACCOUNT.objects.get(ACCOUNTNO = accountNumber)# Obtaining the account detaild matching the submitted ACCOUNTNO
                    context = {
                        "balance": querySet.AMOUNT,
                        "accountNumber": accountNumber,
                    }
                    return render(request, 'bank/view-account-balance.html', context)
                else:
                    raise ValueError('Incorrect Login Details. Please enter correct entries.')
            except ValueError as error:
                    context = {
                        "form": form,
                        "error_message": error.args[0],
                    }
                    return render(request, 'bank/account-balance.html', context)
    context = {
        "form": form,
    }
    return render(request, 'bank/account-balance.html', context)

# Defining a function to perform user authentication
# @param username The username submitted in the login form
# @param password The password submitted in the login form
# @param accountNumber The Account Number submitted in the login form
# @returns A boolean based on whether the submitted login credential matches the entries from the database
def authenticate(username, password, accountNumber):
    try:
        if NEWACCOUNT.objects.filter(ACCOUNTNO=accountNumber).exists():
            querySet = NEWACCOUNT.objects.get(ACCOUNTNO = accountNumber)
            if ((querySet.USERNAME != username) or (querySet.PASSWORD != password)):
                return False
        else:
            return False
    except ValueError as error:
        return False
    else:
        return True

# Defining a view to handle the logic behind requesting a service
def serviceRequest(request):
    form = ServiceForm(request.POST or None)
    if request.method == 'POST': # If the form has been submitted
        form = ServiceForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data
            name = data['USERNAME']
            rawPassword = data['PASSWORD']
            password = hashlib.md5(rawPassword.encode('utf-8')).hexdigest()
            accountNumber = data['ACCOUNTNO']
            serviceType = data['TYPE']
            serviceDate = datetime.datetime.now().date()
            isValidUser = authenticate(username = name, password = password, accountNumber = accountNumber)
            try:
                if isValidUser:
                    user = NEWACCOUNT.objects.get(ACCOUNTNO = accountNumber)
                    new_service_request = CHEQUEBOOK(SERVICENO = 1, SERVICETYPE = serviceType, SERVICEDATE = serviceDate, ACCOUNTNO = user)     # Creating a new service request
                    new_service_request.save()                                  # Saving the new service request details in the databse
                    context = {
                        "accountNumber": accountNumber,
                    }
                    return render(request, 'bank/service-request-success.html', context)
                else:
                    raise ValueError('Incorrect Login Details. Please enter correct entries.')
            except ValueError as error:
                    context = {
                        "form": form,
                        "error_message": error.args[0],
                    }
                    return render(request, 'bank/service-request.html', context)
    context = {
        "form": form
    }
    return render(request, 'bank/service-request.html', context)

# Defining a view to handle the logic behind paying bill online
def payBill(request):
    form = BillForm(request.POST or None)
    if request.method == 'POST': # If the form has been submitted
        form = BillForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data
            name = data['USERNAME']
            rawPassword = data['PASSWORD']
            password = hashlib.md5(rawPassword.encode('utf-8')).hexdigest()
            accountNumber = data['ACCOUNTNO']
            amount = Decimal(data['AMOUNT'])
            biller = data['BILLER']
            isValidUser = authenticate(username = name, password = password, accountNumber = accountNumber)
            try:
                if isValidUser:
                    querySet = NEWACCOUNT.objects.get(ACCOUNTNO = accountNumber)
                    biller = NEWACCOUNT.objects.get(USERNAME = biller)
                    if amount < 0:                                                  # Validating user input
                        raise ValueError('Amount must be a positive number')
                    elif amount > querySet.AMOUNT:                                  # Checking if the user has enough fund in the account
                        raise ValueError('Not sufficient funds in the account!')
                    elif biller == '':                                              # Validating user input
                        raise ValueError('Please select a Biller')
                    querySet.AMOUNT = querySet.AMOUNT - amount                      # Subtracting the amount from the user account
                    querySet.save()
                    biller.AMOUNT = biller.AMOUNT + amount                          # Transferring the amount to the biller account
                    biller.save()
                    context = {
                        "accountNumber": accountNumber,
                    }
                    return render(request, 'bank/pay-bill-success.html', context)
                else:
                    raise ValueError('Incorrect Login Details. Please enter correct entries.')
            except ValueError as error:
                    context = {
                        "form": form,
                        "error_message": error.args[0],
                    }
                    return render(request, 'bank/pay-bill.html', context)
    context = {
        "form": form,
    }
    return render(request, 'bank/pay-bill.html', context)

# Defining a view to handle the logic behind closing an account
def closeAccount(request):
    form = LoginForm(request.POST or None)
    if request.method == 'POST': # If the form has been submitted
        form = LoginForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data
            name = data['USERNAME']
            rawPassword = data['PASSWORD']
            password = hashlib.md5(rawPassword.encode('utf-8')).hexdigest()
            accountNumber = data['ACCOUNTNO']
            isValidUser = authenticate(username = name, password = password, accountNumber = accountNumber)
            try:
                if isValidUser:
                    querySet = NEWACCOUNT.objects.filter(ACCOUNTNO=accountNumber).delete()
                    context = {
                        "accountNumber": accountNumber,
                    }
                    return render(request, 'bank/close-account-success.html', context)
                else:
                    raise ValueError('Incorrect Login Details. Please enter correct entries.')
            except ValueError as error:
                    context = {
                        "form": form,
                        "error_message": error.args[0],
                    }
                    return render(request, 'bank/close-account.html', context)
    context = {
        "form": form,
    }
    return render(request, 'bank/close-account.html', context)
