from django.db import models

# Defining the model for NEWACCOUNT
# Defining the options for the State dropdown menu in the Create Account form
STATE_CHOICES = (
    ('QLD', 'QLD'),
    ('SA', 'SA'),
    ('Tas', 'Tas'),
    ('Vic', 'Vic'),
    ('WA', 'WA'),
)
class NEWACCOUNT(models.Model):
    USERNAME = models.CharField(max_length=40)
    PASSWORD = models.CharField(max_length=256)
    REPASSWORD = models.CharField(max_length=256)
    AMOUNT = models.DecimalField(max_digits=12, decimal_places=2)
    PHONE = models.IntegerField()
    CATEGORY = models.CharField(default='U',max_length=2)
    ADDRESS = models.CharField(max_length=400, null=True)
    STATE = models.CharField(max_length=6, choices = STATE_CHOICES, default = 'QLD', null=True,)
    POSTCODE = models.IntegerField()
    ACCOUNTNO = models.IntegerField(primary_key=True)

# Defining the model for CHEQUEBOOK
# Defining the options for the Servvices dropdown menu in the Request Service form
SERVICE_CHOICES = (
    ('CB', 'Cheque Book'),
)

class CHEQUEBOOK(models.Model):
    ACCOUNTNO = models.ForeignKey(NEWACCOUNT, on_delete=models.CASCADE)
    SERVICENO = models.IntegerField()
    SERVICETYPE = models.CharField(max_length=2, choices = SERVICE_CHOICES)
    SERVICEDATE = models.DateTimeField(null=True)
